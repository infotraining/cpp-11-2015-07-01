#include <iostream>
#include <string>

using namespace std;

//void foo(string s)
//{
//    cout << "foo(string s) - " << s << endl;
//}

void foo(const string& s)
{
    cout << "foo(const string& s) - " << s << endl;
}

void foo(string&& s)
{
    cout << "foo(string&& s) - " << s << endl;
}

//void foo(string& s)
//{
//    cout << "foo(string& s) - " << s << endl;
//}

int main()
{
    string a("leszek");
    foo(a);
    foo(string("ala"));
    return 0;
}

