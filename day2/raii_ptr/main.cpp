#include <iostream>
#include <string>
#include <vector>
#include <memory>

using namespace std;

class Gadget
{
    string name_;
public:
    Gadget(string name) : name_(move(name))
    {
        cout << "ctor of " << name_ << endl;
    }

    ~Gadget()
    {
        cout << "dtor of " << name_ << endl;
    }

    void use()
    {
        cout << "using " << name_ << endl;
    }
};

template<typename T>
class raii_ptr
{
    T* ptr_{nullptr};
public:
    T& operator*() const
    {
        return *ptr_;
    }

    T* operator->() const
    {
        return ptr_;
    }

    raii_ptr() {}

    raii_ptr(T* ptr) : ptr_(ptr)
    {

    }

    raii_ptr(const raii_ptr&) = delete;
    raii_ptr& operator=(const raii_ptr&) = delete;

    raii_ptr(raii_ptr&& rval)
    {
        ptr_ = rval.ptr_;
        rval.ptr_ = nullptr;
    }
    raii_ptr& operator=(raii_ptr&& rval)
    {
        if (this != &rval)
        {
            ptr_ = rval.ptr_;
            rval.ptr_ = nullptr;
        }
        return *this;
    }

    ~raii_ptr()
    {
        // free pointer
        delete ptr_;
    }
};

void fun(int i)
{
    cout << "got i = " << i << endl;
}

void fun(char) = delete;
void fun(double) = delete;

raii_ptr<Gadget> make_gadget(string name)
{
    return raii_ptr<Gadget>(new Gadget(name));
}

void my_fun(const raii_ptr<Gadget>& ptr)
{
    ptr->use();
}

void sink(raii_ptr<Gadget> ptr)
{
    ptr->use();
}

void my_other_fun(raii_ptr<Gadget> ptr, int a)
{
    ptr->use();
    cout << "a = " << a << endl;
}

int my_throwing_fun()
{
    //may throw...
    return 42;
}

int main()
{
    fun(10);
    //fun(3.14);
    //fun('a');
    cout << "Hello World!" << endl;
    {
        cout << "start of scope" << endl;
        Gadget g("one");
        g.use();
        cout << "end of scope" << endl;
    }
    {
        auto_ptr<Gadget> p(new Gadget("two"));
        p->use();

        auto p1 = make_gadget("three");
        //raii_ptr<Gadget> p1(new Gadget("three"));
        p1->use();
    }

    {
        cout << "function usage" << endl;
        auto p = make_gadget("seven");
        my_fun(p);
        cout << "after my_fun" << endl;

    }


    {
        cout << "sink usage" << endl;
        auto p = make_gadget("seven");
        sink(move(p));
        cout << "after sink" << endl;

    }

    {
        cout << "dangers of new" << endl;
        my_other_fun(raii_ptr<Gadget>(new Gadget("something")), my_throwing_fun());
        // potentially leaking..3                 1                      2
        my_other_fun(make_gadget("something"), my_throwing_fun());
    }



    cout << "----- vector" << endl;


    vector<raii_ptr<Gadget>> v;
    v.push_back(new Gadget("four"));
    v.push_back(new Gadget("five"));
    return 0;
}

