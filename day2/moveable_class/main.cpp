#include <iostream>
#include <algorithm>

using namespace std;

class NewClass
{
    int id_;
public:
    NewClass() {}
    // --- rule of five
    ~NewClass() {}
    NewClass(const NewClass&) = delete;
    NewClass& operator=(const NewClass&) = delete;
    NewClass(NewClass&&) = default;
    NewClass& operator=(NewClass&&) = default;
};

class Array
{
    int* tab_;
    size_t size_;
public:
    Array(size_t size, int init) : size_(size)
    {
         cout << "arr ctor" << endl;
         tab_ = new int[size_];
         fill(tab_, tab_+size_, init);
    }

    Array(initializer_list<int> a) : size_(a.size())
    {
        cout << "init list ctor" << endl;
        tab_ = new int[size_];
        copy(a.begin(), a.end(), tab_);
    }

    // no-copy semantic
//    Array(const Array& a) = delete;
    Array(const Array& a) : size_(a.size())
    {
        cout << "copy ctor" << endl;
        tab_ = new int[size_];
        copy(a.begin(), a.end(), tab_);
    }

//    Array& operator=(const Array& a) = delete;
    Array& operator=(const Array& a)
    {
        if (this != &a)
        {
            cout << "copy assignement" << endl;
            tab_ = new int[size_];
            copy(a.begin(), a.end(), tab_);
        }
    }

    // move semantic
    Array(Array&& a) : size_(a.size())
    {
        cout << "move ctor from " << &a << " to " << this << endl;
        tab_ = a.tab_;
        a.size_ = 0;
        a.tab_ = nullptr;
    }

    Array& operator=(Array&& a) noexcept
    {
        if (this != &a)
        {
            cout << "move assignement" << endl;
            tab_ = a.tab_;
            size_ = a.size_;
            a.size_ = 0;
            a.tab_ = nullptr;
        }
        return *this;
    }


    ~Array()
    {
        cout << "arr dtor " << this;
        if (tab_ == nullptr) cout << " on empty"  << endl;
        else cout << endl;
        delete[] tab_;
    }

    size_t size() const
    {
        return size_;
    }

    int& operator[](size_t index)
    {
        return tab_[index];
    }

    int& operator[](size_t index) const
    {
        return tab_[index];
    }

    int* begin() const
    {
        return &tab_[0];
    }

    int* end() const
    {
        return &tab_[size_];
    }

    void print() const
    {
        cout << "[ ";
        for(int i = 0 ; i < size_ ; ++i)
            cout << tab_[i] << ", ";
        cout << "]" << endl;
    }
};

Array gen_array()
{
    return Array(5,-1);
}

void special(const Array& arr)
{
    cout << "Special formating: ";
    arr.print();
}

Array special(Array&& arr)
{
    cout << "Special move formating: ";
    arr.print();
    arr[1] = 100;
    return move(arr);
}

void fun(const Array& arr)
{
    cout << "fun(const)" << endl;
}

//void fun(Array&& arr)
//{
//    cout << "fun(&&)" << endl;
//}

//void fun(Array arr)
//{
//    cout << "fun(val)" << endl;
//}

int main()
{
    Array a(10, 1);
    Array c{1,2,3,4,5};
    //Array d(c);
//    Array b(gen_array()); // works... just because of optimization (without -fno-elide-constructors)
//    Array arr_z = special(Array(10,-1));
//    special(c);       // special(const&)
//    c = special(move(c)); // special(&&)

//    vector<Array> v;
//    v.reserve(100);
//    v.push_back(Array{1,2,3,4});
//    v.push_back(move(a));
//    a.print(); //?
//    v.push_back(Array(5,-2));
//    //Array b = a;
//    for (const auto& el : v)
//        el.print();

    fun(a);
    fun(Array{1,2,3});

    cout << "---------end -----------" << endl;
    return 0;
}

