#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Gadget
{
    int id_;
    string name_;
public:
    Gadget(int id) : id_(id)
    {
        cout << "ctor " << id_ << endl;
    }

    Gadget(string name) : name_(move(name))
    {
    }

    ~Gadget()
    {
        cout << "dtor " << id_ << endl;
    }

    Gadget(const Gadget& g) : id_(g.id_)
    {
        cout << "copy ctor " << id_ << endl;
    }

    Gadget(Gadget&& g) = default; /*noexcept : id_(g.id_)
    {
        cout << "move ctor " << id_ << endl;
    }*/

    Gadget& operator=(const Gadget& g)
    {
        id_ = g.id_;
        cout << "assignement op " << id_ << endl;
    }

    Gadget& operator=(Gadget&& g) noexcept
    {
        id_ = g.id_;
        cout << "move assignement op " << id_ << endl;
    }

};

void input(const Gadget& g)
{
    cout << "input(const Gadget& g)" << endl;
}

void input(Gadget&& g)
{
    cout << "input(Gadget&& g)" << endl;
}

Gadget output()
{
    cout << "output()" << endl;
    return Gadget(1);
}

int main()
{
//    Gadget g = output();
//    //Gadget g2 = g;
//    input(Gadget(2));
//    input(g);

    string s("ola");
    Gadget g1("ala");
    Gadget g2(s);

    vector<Gadget> v;
    v.push_back(Gadget(1));
    v.push_back(Gadget(2));
    v.push_back(Gadget(3));

    cout << "---------end----------" << endl;
    return 0;
}

