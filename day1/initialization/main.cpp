#include <iostream>
#include <vector>
#include <boost/type_index.hpp>
#include <string>

using namespace std;

class Gadget
{
    int id;
public:
    Gadget() : id(0)
    { cout << "ctor" << endl;}
    void print()
    { cout << id << endl;}
};

class Storage
{
    vector<int> v_;
public:
    Storage(initializer_list<int> l) : v_(l)
    {
//        for(auto& el : l)
//            v_.push_back(el);
    }

    vector<int> data()
    {
        return v_;
    }
};

int get_val()
{
    return 0;
}

class Point
{
    int x{5};
    int y{5};
public:
    Point() : x{get_val()}, y{get_val()}
    {
        cout << "ctor()" << endl;
    }

    Point(int x, int y) : x(x), y(y)
    {
        cout << "ctor(int,int)" << endl;
    }

    Point(initializer_list<int> params)
    {
        cout << "ctor(params)" << endl;
        x = *begin(params);
        y = *(begin(params)+1);
    }

    void print()
    {
        cout << "pos: " << x << ", " << y << endl;
    }
};

struct Pack
{
    string a;
    int b;
    Pack(string a, int b) : a(a), b(b)
    {
        cout << "pack ctor" << endl;
    }
};

struct AnotherPack
{
    int a[2];
    int b;
};

int main()
{
    // C++ 98

    int a; // not initalized, value unknown
    Gadget g;
    // Gadget g(); does not work - function declaration
    int tab[] = {1,2,3};
    g.print();
    Pack t("ala", 1);
    // vector<int> v = {1,2,3}; does not compile in c++98

    // C++11
    int b{}; // initialized
    auto c = 0;
    auto d = int{};

    Gadget g2{}; // calls constructor
    g2.print();
    int tab2[] = {1,2,3};
    vector<int> v;
    v = {1,2,3};

    //auto something = {1,2,3, "ala"}; // nothing
    auto something = {1,2,3}; // std::initalizer_list<int>
    for (auto& el : something)
        cout << el << ",";
    cout << endl;
    cout << "something = " ;
    cout << boost::typeindex::type_id_with_cvr<decltype(something)>().pretty_name() << endl;

    Storage s{1, 2, 3};
    for(auto& el : s.data())
        cout << el << endl;

    // ctor parameters

    Point p1;
    p1.print();

    Point p2(1,2); // ctor(int,int)
    p2.print();

    Point p3{1,2}; // ctor(initializer_list)
    p3.print();


    // well

    vector<string> v2{10, "ala"};
    for(auto& el : v2)
        cout << el << ", ";
    cout << endl;

    vector<int> v3(10,20);
    for(auto& el : v3)
        cout << el << ", ";
    cout << endl;

    // calling ctor

    Pack p{"ala ma kota", 20};
    cout << p.a << " " << p.b << endl;

    AnotherPack apack{1,2,3};
    cout << apack.a[1] << endl;


    return 0;
}


















