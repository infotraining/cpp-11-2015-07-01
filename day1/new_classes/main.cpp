#include <iostream>

using namespace std;

class Gadget
{
    int id_;
public:

    //Gadget(const Gadget& g) {} // C++98
    Gadget(int id) : id_(id)
    {

    }

    Gadget(const Gadget&) = delete;
    Gadget& operator=(const Gadget&) = delete;
    Gadget() = default;

    void print()
    {
        cout << id_ << endl;
    }


};

int main()
{
    Gadget g{10};
    g.print();
    Gadget g2{};
    //Gadget g3 = g;
    //g3.print();
    return 0;
}

