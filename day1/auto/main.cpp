#include <iostream>
#include <vector>
#include <boost/type_index.hpp>

using namespace std;

template<typename T1, typename T2>
auto mymultiply(T1 a, T2 b) -> decltype(a*b) // C++11 and C++14
{
    return a*b;
}

template<typename T1, typename T2>
decltype(auto) mymultiply14(const T1& a, const T2& b)
{
    return a*b;
}

auto my_fun() -> long double
{
    return 3.14;
}

auto main() -> int
{
    cout << "Hello World!" << endl;

    int a = 0; // c++98
    auto b = 0; // c++11 forced initialization

    auto res = my_fun(); // my_fun is called

    decltype(my_fun()) uninitialized_res; // my_fun is not called
    uninitialized_res = my_fun();

    double res_d = my_fun(); // c++98
    auto res_double = static_cast<double>(my_fun()); // c++11

    cout << res << endl;

    auto v = vector<int>{1,2,3,4};
    auto s = v.size();
    cout << mymultiply(3, 5) << endl;

    // doubtfull sides of auto

    const int& ref_a = a;
    auto a_ref_a = ref_a;  // type of a_ref_a = int!!!!

    cout << "ref_a = ";
    cout << boost::typeindex::type_id_with_cvr<decltype(ref_a)>().pretty_name() << endl;
    cout << "a_ref_a = ";
    cout << boost::typeindex::type_id_with_cvr<decltype(a_ref_a)>().pretty_name() << endl;

    for(auto& el : v)
        el = el*2;

    for(const auto& el : v)
        cout << el << ", ";
    cout << endl;

    auto& int_el = v[1];

    vector<bool> vb = {true, true};

    auto bool_el = vb[1];
    auto true_bool = static_cast<bool>(vb[1]);
    cout << "bool_el = ";
    cout << boost::typeindex::type_id_with_cvr<decltype(bool_el)>().pretty_name() << endl;

    for(auto el : vb)
        el = false;

    for(const auto& el : vb)
        cout << el << ", ";
    cout << endl;

    // joke

    int tab[] = {1,2,3};
    cout << tab[1] << endl;
    cout << *(tab+1) << endl;
    cout << 1[tab] << endl;

    return 0;
}





















