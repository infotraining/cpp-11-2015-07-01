#include <iostream>
#include <vector>

using namespace std;

int main()
{
    vector<int> v{1,2,3,4,5};

    for(auto& element : v)
    {
        v.push_back(1);
        v.push_back(1);
        cout << element << endl;
    }

    for(auto it = begin(v); it != end(v) ; ++it)
    {
        auto& element = *it; // return reference to element;
        cout << element << endl;
    }


    vector<bool> vb{1,1,1};

    for(auto it = begin(vb); it != end(vb) ; ++it)
    {
        //const auto& element = *it; // return reference to element;
        //const bool& element = *it;
        _Bit_reference element = *it;
        cout << element << endl;
    }


    // funny mistake

    vector<int> v1{1,2,3};
    vector<int> v2{4,5,6};

    for(auto it = v1.begin() ; it != v2.end() ; ++it)
        cout << *it << ", ";
    cout << endl;

    /*
    vector<int> v2;
    v2.reserve(100);
    v2.push_back(1);
    v2.push_back(1);
    v2.push_back(1);
    cout << "cap: " << v2.capacity();
    cout << " size: " << v2.size() << endl;*/

    return 0;
}

