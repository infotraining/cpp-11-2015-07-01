#include <iostream>
#include <vector>

using namespace std;

struct Gadget
{
    int id;
    Gadget(int id) : id(id) {}
};

template<typename Cont>
auto find_null(const Cont& c) -> decltype(begin(c))
{
    for(auto it = begin(c) ; it != end(c) ; ++it)
    {
        if (*it == nullptr) return it;
    }
    return end(c);
}

int main()
{
    cout << "Hello World!" << endl;
    vector<Gadget*> v = {new Gadget(1), new Gadget(2), nullptr, new Gadget(3)};
    Gadget* tab[] = {new Gadget(1), new Gadget(2), nullptr, new Gadget(3)};

    //for (auto it = begin(v) ; it != end(v) ; ++it)
    for (auto it = begin(v) ; it != find_null(v) ; ++it)
        cout << (*it)->id << endl;

    for (auto it = begin(tab) ; it != find_null(tab) ; ++it)
        cout << (*it)->id << endl;

    return 0;
}

