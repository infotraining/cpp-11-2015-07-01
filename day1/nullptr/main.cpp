#include <iostream>

using namespace std;

void f_ptr(int* ptr)
{
    if(ptr != 0)
    {
        cout << "f(int* )" << endl;
    }
    else
    {
        cout << "f(NULL)" << endl;
    }
}

void f_ptr(int a)
{
    cout << "f_ptr(int)" << endl;
}

int main()
{
    cout << "Hello World!" << endl;
    int a = 0;
    f_ptr(&a);
    //f_ptr(NULL);
    f_ptr(static_cast<int*>(NULL));
    f_ptr(123);
    f_ptr(nullptr);

    return 0;
}

