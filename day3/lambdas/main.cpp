#include <iostream>
#include <functional>
#include <vector>
#include <algorithm>
#include <string>
#include <memory>
#include <boost/type_index.hpp>

using namespace std;

void fun(int i)
{
    cout << "fun(i) - " << i << endl;
}

void action(void(*f)(int), int val)
{
    f(val);
}

void modern_action(std::function<void(int)> f, int val)
{
    f(val);
}

struct Functor
{
    int z{};
    Functor(int z) : z(z)
    {

    }

    void operator()(int i)
    {
        cout << "Functor(i) - " << i << endl;
    }
};

auto make_adder(int a) -> std::function<int(int)>
{
    int tmp = a;
    return [tmp](int b) { return tmp + b; };
}

template<typename A>
auto make_generalized_adder(A a)
{
    return [a](auto b) { return a + b; };
}

void sink(unique_ptr<string> s)
{
    if(s)
        cout << *s << endl;
}

int main()
{
    Functor f(22);
    fun(10);
    f(12);
    action(&fun, 11);
    //action(&f, 13); // function pointers does not work with functors
    modern_action(&fun, 13);
    modern_action(f, 14);
    modern_action([] (int n) { cout << "lambda - " << n << endl;}, 15);

    // lmabda in algorithms
    vector<int> v{1,2,3,4,5,6,7,8,9};
    vector<int> res;

    int n = 3;
    copy_if(begin(v), end(v), back_inserter(res),
            [n](int el) { return el % n;});

    for(const auto& el : res)
        cout << el << ", ";
    cout << endl;

    // lambda and catch

    auto vl = [res] () mutable -> vector<int>  {
        res[3] = 2;
//        for (auto& el : res)
//            cout << el << ";; ";
//        cout << endl;
        return res;
    };

    auto mod_vec = vl();

    // type of lambda
    int z{99};
    auto l1 = + [] (int a) { cout << "my lambda1 nr " << a << endl;};
    Functor l2f(z);

    auto l2 = [z] (int a) { cout << "my lambda2 nr " << a << " and " << z << endl;};
    auto l3 = [&z] (int a) { cout << "my lambda3 nr " << a << " and " << z << endl;};
    std::function<void(int)> lf = [] (int a) { cout << "my lambda nr " << a << endl;};

    cout << boost::typeindex::type_id_with_cvr<decltype(l1)>().pretty_name() << endl;
    cout << boost::typeindex::type_id_with_cvr<decltype(l2)>().pretty_name() << endl;
    cout << boost::typeindex::type_id_with_cvr<decltype(l3)>().pretty_name() << endl;
    cout << boost::typeindex::type_id_with_cvr<decltype(lf)>().pretty_name() << endl;

        action(l1, 22);  // void(*)(int)
    //action(l2, 22);
    z = 100;
    l1(20);
    l2(21);
    l3(21);
    lf(22);

    auto add5 = make_adder(5);
    cout << "5 + 10 = " << add5(10) << endl;
    auto add7 = make_adder(7);
    cout << "7 + 10 = " << add7(10) << endl;

    // since C++14

    auto gadder = make_generalized_adder(5);
    auto gadder_sting = make_generalized_adder("ala"s);
    cout << "5 + 10 = " << gadder(10) << endl;
    cout << "ola + ala = " << gadder_sting("ola"s) << endl;

    auto p = make_unique<string>("leszek"s);

    auto luniq = [p=move(p)] () mutable {
        sink(move(p));
    };
    //std::function<void()> flu(move(luniq));
    luniq();
    luniq(); // not working




    return 0;
}









