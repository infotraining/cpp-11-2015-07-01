#include <iostream>
#include <array>

using namespace std;

constexpr int cube(int n)
{
    return n*n*n;
}

constexpr size_t factorial(int n)
{
    return (n == 0) ? 1 : n * factorial(n-1);
}

int main()
{
    const size_t EDGE = 9;
    const size_t CUBE = EDGE*EDGE*EDGE;
    const size_t FAC_9 = factorial(9);
    array<int, cube(EDGE)> arr{1,2,3};
    array<int, factorial(EDGE)> arr2{1,2,3};
    cout << cube(100) << endl;
    cout << factorial(9) << endl;
    cout << FAC_9 << endl;
    return 0;
}

