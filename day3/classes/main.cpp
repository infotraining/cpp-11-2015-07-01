#include <iostream>
#include <memory>

using namespace std;

class Base {
public:
    virtual void doWork() final
    {
        cout << "do work from Base" << endl;
    }

    virtual void mf1(int i) const
    {
        cout << "Base::mf1" << endl;
    }

    virtual void mf2(const int& i)
    {
        cout << "Base::mf2" << endl;
    }
};

class Derived : public Base
{
//    void doWork()
//    {
//        cout << "do work from Derived" << endl;
//    }

    virtual void mf1(int i) const override
    {
        cout << "Derived::mf1" << endl;
    }

    virtual void mf2(const int& i) override
    {
        cout << "Derived::mf2" << endl;
    }

};

int main()
{
    cout << "Hello World!" << endl;
    unique_ptr<Base> p = make_unique<Derived>();
    p->mf1(10);
    p->mf2(10);
    return 0;
}

