#include <iostream>
#include <string>
#include <vector>
#include <memory>

using namespace std;

class Gadget
{
    int id_;
    string name_;
public:
    Gadget(int id, string name) : id_(id), name_(name)
    {
        cout << "ctor " << id << " " << name << endl;
    }

    void print()
    {
        cout << "name: " << name_;
        cout << " id: " << id_ << endl;
    }
};

class AnotherGadget
{
    int value_;
public:
    AnotherGadget(int value) : value_(value)
    {
        cout << "ctor " << value << endl;
    }

};

template<typename G, typename... A>
unique_ptr<G> make_unique(A&&... p)
{
    return unique_ptr<G>{new G{std::forward<A>(p)...}};
}

template<typename G, typename P1, typename P2>
unique_ptr<G> make_gadget(P1 p1, P2 p2)
{
    return unique_ptr<G>{new G{p1, p2}};
}


int main()
{
    unique_ptr<Gadget> p = unique_ptr<Gadget>{new Gadget{10, "ten"}};
    auto p2 = make_unique<Gadget>(11, "eleventeen");
    p->print();
    p2->print();
    auto p3 = make_unique<AnotherGadget>(12);

    vector<Gadget> v;
    v.push_back(Gadget(12, "twelve"));
    v.emplace_back(13,"thirteen");
    v.emplace_back(Gadget(14, "fourteen"));

    vector<unique_ptr<Gadget>> vp;
    vp.emplace_back(make_unique<Gadget>(15, string("fifteen")));
    vp.emplace_back(new Gadget(16, "sixteen"));

    return 0;
}

