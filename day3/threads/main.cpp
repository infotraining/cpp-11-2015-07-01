#include <iostream>
#include <thread>
#include <future>

using namespace std;

int answer(int id)
{
    this_thread::sleep_for(500ms);
    if(id == 13) throw std::logic_error("bad luck");

    return 42;
}

void process(future<double> lazy)
{
    cout << "processing" << endl;
    cout << "result = " << lazy.get() << endl; // here function starts
}

int main()
{
    cout << "Hello World!" << endl;
    //cout << "Ultimate answer = " << answer(666) << endl;
    future<int> res = async(launch::async, &answer, 13);
    cout << "main loop" << endl;
    res.wait();
    try
    {
        cout << "Ultimate answer = " << res.get() << endl;
    }
    catch(logic_error& err)
    {
        cout << "Got error " << err.what() << endl;
    }

    future<double> param = async(launch::deferred, [] (int n) { return 3.14*n;}, 2);
    process(move(param));   // not executed here
    process(async(launch::deferred, [] () { return 123.456;}));// not executed here

    return 0;
}

