#include <iostream>
#include <string>
#include <tuple>

using namespace std;

// counter
template<typename...A>
struct Counter;

template<typename first, typename...A>
struct Counter<first, A...>
{
    constexpr static int size = 1 + Counter<A...>::size;
};

template<>
struct Counter<>
{
    constexpr static int size = 0;
};

// adder
template<typename T>
T adder(T one)
{
    return one;
}

template<typename T, typename... A>
T adder(T first, A... a)
{
    return first + adder(a...);
}

int main()
{
    cout << adder(1,2,3) << endl;
    cout << adder(1,2,3,4,5,6) << endl;

    cout << Counter<int, double, char>::size << endl; // should return 3

    tuple<int, double, string> t(1, 3.14, "leszek");
    cout << get<2>(t) << endl;
    cout << get<1>(t) << endl;
    cout << get<0>(t) << endl;

    return 0;
}

