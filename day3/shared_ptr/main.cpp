#include <iostream>
#include <memory>
#include <string>

using namespace std;

/*class Human - bad
{
    shared_ptr<Human> spouse_;
    string name_;
public:
    Human(string name) : name_(name)
    {
        cout << "ctor of human named " << name_ << endl;
    }

    ~Human()
    {
        cout << "dtor of human named " << name_ << endl;
    }

    void set_partner(shared_ptr<Human> partner)
    {
        spouse_ = partner;
    }

    void print()
    {
        cout << "my name is " << name_;
        if(spouse_)
            cout << " and my spouse is " << spouse_->name_;
        cout << endl;
    }
}; */

class Human
{
    weak_ptr<Human> spouse_;
    string name_;
public:
    Human(string name) : name_(name)
    {
        cout << "ctor of human named " << name_ << endl;
    }

    ~Human()
    {
        cout << "dtor of human named " << name_ << endl;
    }

    void set_partner(weak_ptr<Human> partner)
    {
        spouse_ = partner;
    }

    void print()
    {
        cout << "my name is " << name_;
        shared_ptr<Human> spouse = spouse_.lock();
        if(spouse)
            cout << " and my spouse is " << spouse->name_;
        cout << endl;
    }
};


int main()
{
    auto h2 = make_shared<Human>("Eve");
    auto h1 = make_shared<Human>("Adam");
    h1->set_partner(h2);
    h2->set_partner(h1);
    h1->print();
    h2->print();
    return 0;
}

